package app;
import java.io.IOException;

import axillary.GenerateFormulaFile;

// generate a file with formulas
public class GenerateFormulaFileApp {

	public static void main(String[] args) {
		
		final int formulaNumber = 500;
		final int counterNumber = 100;
		final int maxOperandNumber = 10;
		final String fileName = "formulas.txt";

		try {
			GenerateFormulaFile generator = new GenerateFormulaFile(fileName, formulaNumber, counterNumber, maxOperandNumber);
			generator.generateFormulaFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
