package app;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import axillary.GenerateFormulaFile;
import computing.PackComputing;
import computing.PackComputingThread;

import java.util.Properties;
import java.util.Scanner;
import java.util.concurrent.ConcurrentHashMap;


public class Boot {

    private static Logger logger = LogManager.getLogger(Boot.class);
    
	public static void main(String[] args) throws InterruptedException {
				
		// load the config
		Properties props = new Properties();
		try {
			props.load(new FileInputStream(new File("config/config.ini")));
		} catch (IOException e) {
			logger.error("Config file is not loaded", e);
			e.printStackTrace();
			return;
		}

		final int counterNumber = 100;
		final String fileName = props.getProperty("FILE_PATH");
		
		// initialize a map of variables (counters)
		Map<String, Double> countersMap = initializeCounters(counterNumber);
		// start processing
		PackComputingThread thread = new PackComputingThread(fileName, countersMap);
		thread.start();

		// wait for 'exit'
		try(Scanner scan = new Scanner(System.in)) {
			while(true) {
				System.out.println("Write 'exit' to close the application");
				String line = scan.nextLine();
				if(line != null) {
					if(line.toLowerCase().equals("exit"))
						break;
				}
			}	
		}
		
		thread.terminate();
		thread.join();
		System.out.println("The app has been finished");
		logger.info("The app has been finished");
	}
	

	/***
	 * initialize counters with random values
	 * @param counterNumber
	 * @return
	 */
	public static Map<String, Double> initializeCounters(final int counterNumber) {
		Map<String, Double> variablesMap = new ConcurrentHashMap<String, Double>(counterNumber);
		for(int i=0; i<counterNumber; i++) {
			String variableName = GenerateFormulaFile.VARIABLE_TEMPLATE + i;
			variablesMap.put(variableName, new Double(i));
		}
		return variablesMap;
	}
}
