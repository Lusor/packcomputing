package axillary;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

/***
 * this class evaluate an arithmetic expression written in JavaScript
 * @author vb7mo
 *
 */
public class ScriptFormulaComputer implements IFormulaComputer {
	
	private ScriptEngineManager mgr;
	private ScriptEngine engine;
	
	public ScriptFormulaComputer () {
		mgr = new ScriptEngineManager();
	    engine = mgr.getEngineByName("JavaScript");
	}
	  
	@Override
	public Double compute(String str) {
	    try {
	    	Double res = (Double) engine.eval(str);
	    	return res;
		} catch (ScriptException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}	
}
