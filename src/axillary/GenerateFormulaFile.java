package axillary;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

/***
 * class for generating a file with formulas
 * @author vb7mo
 *
 */
public class GenerateFormulaFile {

	public static final String VARIABLE_TEMPLATE = "counter";
	private final char[] OPERATIONS = new char[] {'+', '-', '*', '/'};
	private final String[] UNARY_OPERATIONS = new String[] {"Math.pow(xxx, 2)", "Math.abs(xxx)", "Math.sqrt(xxx)"};
	private String fileName = null;
	private int formulasNumber = 10;
	private int countersNumber = 10;
	private int maxOperandNumber = 5;
	
	public GenerateFormulaFile(String fileName, int formulasNumber, int countersNumber, int maxOperandNumber) {
		this.fileName = fileName;
		this.formulasNumber = formulasNumber;
		this.countersNumber = countersNumber;
		this.maxOperandNumber = maxOperandNumber;
		
		if(fileName == null || fileName.trim().isEmpty())
			throw new IllegalArgumentException("Argument 'filename' cannot be empty");
	}
	
	public void generateFormulaFile() throws IOException {
		ArrayList<String> formulaList = new ArrayList<String>(formulasNumber);
		for(int i=0; i<formulasNumber; i++) {
			formulaList.add(generateFormula());
		}
		writeToFile(formulaList);
	}
	
	private String generateFormula() {
		Random rand = new Random();
		int operandsNumber = rand.nextInt(maxOperandNumber) + 2;
		StringBuilder builder = new StringBuilder();
		for(int i=0; i<operandsNumber; i++) {		
			int counterID = rand.nextInt(countersNumber);
			int operationID = rand.nextInt(OPERATIONS.length);
			String operandName = VARIABLE_TEMPLATE + counterID;
			
			String operand = getRandomUnaryOperation(rand, operandName);
			builder.append(operand);
			if(i != operandsNumber-1)
				builder.append(OPERATIONS[operationID]);
		}
		return builder.toString();
	}
	
	private void writeToFile(ArrayList<String> formulaList) throws IOException {
		BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new FileWriter(this.fileName));
			for(int i=0; i<formulaList.size(); i++) {
				writer.write(formulaList.get(i));
				writer.newLine();
			}
		} finally {
			writer.close();
		}
	}
	
	private String getRandomUnaryOperation(Random rand, String operand) {
		float chance = rand.nextFloat();
		if(chance < 0.25f) {
			int operationID = rand.nextInt(UNARY_OPERATIONS.length);
			String operation = UNARY_OPERATIONS[operationID];
			return operation.replaceAll("xxx", operand);
		} else
			return operand;
			
	}
}
