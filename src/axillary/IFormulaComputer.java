package axillary;

/**
 * interface for computing a formula
 * @author vb7mo
 *
 */
public interface IFormulaComputer {
	Double compute(String str);
}
