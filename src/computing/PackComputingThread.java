package computing;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import app.Boot;

public class PackComputingThread extends Thread{

    private static Logger logger = LogManager.getLogger(Boot.class);
	final String fileName;
	final Map<String, Double> countersMap;
	private volatile boolean running = true;
	
	public PackComputingThread(String fileName,  Map<String, Double> countersMap) {
		this.fileName = fileName;
		this.countersMap = countersMap;
	}
	
	@Override
	public void run() {
		
		try {
			PackComputing app = new PackComputing(fileName, countersMap);
			
			while(running) {
				app.performProcessing();
				Thread.sleep(5000);
			}

		} catch(Exception exc) {
			logger.error("Error while processing", exc);
		}
	}
	
	public void terminate() {
    	running = false;
	}
}
