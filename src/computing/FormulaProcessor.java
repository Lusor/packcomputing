package computing;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import axillary.IFormulaComputer;
import axillary.StringLengthComparator;

public class FormulaProcessor {

	private IFormulaComputer computer;
	private Map<String, Double> countersMap = null; 
	private StringLengthComparator stringComparator = new StringLengthComparator();
	
	public FormulaProcessor(Map<String, Double> variablesMap) {
		this.countersMap = variablesMap;
	}
	
	/***
	 * set formula computer
	 * @param computer
	 */
	public void setFormulaComputer(IFormulaComputer computer) {
		this.computer = computer;
	}
	
	/***
	 * evaluate formula using values of its variables
	 * @param inputStr
	 * @return
	 */
	public Double process(String inputStr) {
		if(inputStr == null || inputStr.trim().isEmpty())
			throw new IllegalArgumentException("Input string cannot be null or empty"); 
		
		// find all variables in the formula
		Pattern pattern = Pattern.compile("counter[0-9]+");
		Matcher matcher = pattern.matcher(inputStr);
		Set<String> operandsSet = new HashSet<String>();
		while(matcher.find()) {
			String groupStr = matcher.group();
			//System.out.println(groupStr);
			operandsSet.add(groupStr);
		}
		// sort variables by their lengths
		String[] operandsArr = operandsSet.toArray(new String[operandsSet.size()]);
		Arrays.sort(operandsArr, stringComparator);

		// replace variables with their values
		// arithmetic expression should not contain variables (counters)
		String arithmeticExpression = substituteVariables(inputStr, operandsArr);
		
		Double res = computer.compute(arithmeticExpression);
		return res;
	}
	
	private String substituteVariables(String inputStr, String[] operands) {
		if(inputStr == null)
			return null;
		String arithmeticExpression = new String(inputStr);
		for(int i=operands.length-1; i>=0; i--) {
			String counterName = operands[i];
			if(counterName == null)
				continue;
			Double value = this.countersMap.get(counterName);
			arithmeticExpression = arithmeticExpression.replaceAll(counterName, value.toString());
		}
		return arithmeticExpression;
	}
	
	
	
}
