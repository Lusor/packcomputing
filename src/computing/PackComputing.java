package computing;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import axillary.IFormulaComputer;
import axillary.ScriptFormulaComputer;


public class PackComputing {

    private static Logger logger = LogManager.getLogger(PackComputing.class);
    
	// for loading file with formulas 
	FormulaFileLoader loader = null;
	// for processing formulas
	FormulaProcessor formProc = null;
	// for computing arithmetic expressions
	IFormulaComputer scriptComputer = null;
	
	String fileName = null;
	// for storing formulas
	Map<String, Double> formulaMap = new HashMap<>();
	// last update time
	long lastUpdateTime = 0;
	
    public PackComputing(String fileName, Map<String, Double> countersMap) {
    	this.fileName = fileName;
		try {
			formProc = new FormulaProcessor(countersMap);
			scriptComputer = new ScriptFormulaComputer();
			formProc.setFormulaComputer(scriptComputer);	
			
			loader = new FormulaFileLoader(fileName);
		} catch (Exception exc) {
			logger.error("Error during initializing", exc);
			return;
		}
    }
    
    public void performProcessing () throws InterruptedException {

		try {
			long updateTime = loader.checkUpdateTime();
			if(updateTime > this.lastUpdateTime) {
				logger.info(String.format("The file %s has been changed", fileName));
				
				Set<String> newFormulas = null;
				// first load the modified file with formulas, remove the old ones and add the new ones 
				try {
					newFormulas = loader.updateFormulaMap(formulaMap);
				} catch (IOException e) {
					logger.error(String.format("Cannot read the file %s", fileName), e);
					e.printStackTrace();
				}
				
				// then compute only the newly added formulas
				if(newFormulas != null) {
					computeForFormulaSet(formProc, formulaMap, newFormulas);
				}
				// the update is processed
				this.lastUpdateTime = updateTime;
				
				logger.info(String.format("Processed %d new formulas", newFormulas==null ? 0:newFormulas.size()));
				logger.info(String.format("Processing has been finished: %s", fileName));
			}
		} catch(Exception exc) {
			logger.error(String.format("Error during processing file %s", fileName), exc);
		}
    }
    

	/***
	 * compute values of formulas form the set of formulas
	 * @param formProc
	 * @param formulaMap
	 * @param newFormulas
	 */
	private static void computeForFormulaSet(FormulaProcessor formProc, Map<String, Double> formulaMap,
			Set<String> newFormulas) {
		
		if(newFormulas == null) {
			logger.warn("Formula set is null.");
		}
		Iterator<String> iter = newFormulas.iterator();
		while(iter.hasNext()) {
			String formula = iter.next();
			Double res = formProc.process(formula);
			if(res != null && Double.isFinite(res) && !Double.isNaN(res)) {
				
				//System.out.println(String.format("Result = %f", res));
				if(formulaMap.containsKey(formula)) {
					formulaMap.put(formula, res);
				} else {
					logger.warn(String.format("The formula is not found: %s", formula));
				}
			} else {
				logger.error(String.format("The formula is not computable: %s", formula));
			}
		}
	}
}
