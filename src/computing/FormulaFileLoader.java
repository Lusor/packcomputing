package computing;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/***
 * loading a file with formulas
 * @author vb7mo
 *
 */
public class FormulaFileLoader {

	private String fileName = null;
	public FormulaFileLoader(String fileName) {
		this.fileName = fileName;
		
		if(fileName == null || fileName.trim().isEmpty())
			throw new IllegalArgumentException("Argument 'filename' cannot be empty");
	}
	
	/***
	 * check the date of the file
	 * @return
	 * @throws Exception 
	 */
	public long checkUpdateTime() throws FileNotFoundException {
		File file = new File(fileName);
		if(!file.exists()) {
			throw new FileNotFoundException("checkUpdateTime: File does not exist");
		}
		return file.lastModified();
	}
	
	/***
	 * load formulas from the file
	 * @return
	 * @throws IOException
	 */
	private Set<String> loadFormulasSet() throws IOException {
		Set<String> set = new HashSet<>();
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(this.fileName));
			String formulaLine = null;
			while((formulaLine = br.readLine()) != null) {
				set.add(formulaLine);
			}
		} finally {
			br.close();
		}
		return set;
	}
	
	/***
	 * update a collection of formulas from a file (remove old one, add new ones)
	 * @param curMap
	 * @return
	 * @throws IOException
	 */
	public Set<String> updateFormulaMap(Map<String, Double> curMap) throws IOException {
		Set<String> updatedFormulaSet = loadFormulasSet();
		
		// remove elements (old formulas) that are not in updatedFormulaSet
		curMap.keySet().retainAll(updatedFormulaSet);
		
		// leave only new formulas and add new formulas
		updatedFormulaSet.removeAll(curMap.keySet());
		Iterator<String> iter = updatedFormulaSet.iterator();
		while(iter.hasNext())
			curMap.put(iter.next(), 0.0d);
		
		// return only formulas to be computed
		return updatedFormulaSet;
	}	
	
}
