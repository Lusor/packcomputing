The program processes a file with formulas what are written in JavaScript (evaluation is implemented using ScriptEngine).
When the file with formulas is modified, the program should check changes and and compute new formulas.

Input data: a file with formulas, generated counters 
OutputData: log file, updated counters

To finish the program: type 'exit', press Enter and wait.

Processing order:
1. load file with formulas and put them into the map
2. For each formula, we replace counters names (as operands of formulas) with their values
3. Compute arithmetic expressions and put result into the map (logging errors)
4. Check again if the file data has been changed: if there are 'old' formulas - remove them, only for new formulas we compute thier values

Project structure:
/scr/app/Boot.java - main starting point
/scr/app/GenerateFormulaFileApp.java - for generating formula list
/scr/computing - a package related to formula processing and computing
/scr/axillary - axillary classes

Developed using Java 1.8 and Maven repositories (log4j, jUnit)

Errors are logged using Log4j:
[INFO ] 2018-12-09 10:40:08.271 [main] PackComputingApp - Processed 500 new formulas
[INFO ] 2018-12-09 10:40:08.271 [main] PackComputingApp - Processing has been finished: formulas.txt
[INFO ] 2018-12-09 10:40:28.274 [main] PackComputingApp - The file formulas.txt has been changed
[INFO ] 2018-12-09 10:40:28.293 [main] PackComputingApp - Processed 2 new formulas
[INFO ] 2018-12-09 10:40:28.293 [main] PackComputingApp - Processing has been finished: formulas.txt

P.S. Tests are written only to check formula computing (due to the lack of time).