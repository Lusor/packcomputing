package computing.test;


import static org.junit.Assert.assertEquals;

import java.util.Map;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import app.Boot;
import axillary.IFormulaComputer;
import axillary.ScriptFormulaComputer;
import computing.FormulaProcessor;

/***
 * test the formula processor class
 * @author vb7mo
 *
 */
public class FormulaCheckTest  {
	
	// for processing formulas
	FormulaProcessor formProc = null;
	// for computing arithmetic expressions
	IFormulaComputer scriptComputer = null;
	
	public FormulaCheckTest() {

		Map<String, Double> countersMap = Boot.initializeCounters(100);
		formProc = new FormulaProcessor(countersMap);
		scriptComputer = new ScriptFormulaComputer();
		formProc.setFormulaComputer(scriptComputer);	
	}
	
	@Rule
	public ExpectedException exception = ExpectedException.none();
	  
	@Test
	public void throwsIllegalArgumentExceptionIfNull() {
	    exception.expect(IllegalArgumentException.class);
		formProc.process(null);
	}
	
	@Test
	public void throwsIllegalArgumentExceptionIfEmpty() {
	    exception.expect(IllegalArgumentException.class);
		formProc.process(null);
	}
	  
	@Test
	public void testNormalCase() {
		String formula = "counter1 + counter2";
		Double res = formProc.process(formula);
		assertEquals(res, new Double(3.0d));
	}
	
	@Test
	public void testIfDivideZero() {
		String formula = "counter1/0";
		Double res = formProc.process(formula);
		assertEquals(res.isInfinite(), true);
	}
	
	@Test
	public void testIfNotCounter() {
		String formula = "not_a_counter + 1";
		Double res = formProc.process(formula);
		assertEquals(res, null);
	}
	
	@Test
	public void testIfAnyString() {
		String formula = "any string";
		Double res = formProc.process(formula);
		assertEquals(res, null);
	}
	
	@Test
	public void testIfCorrectArithmExpression() {
		String formula = "2 + Math.pow(2, 2) + Math.abs(-2)";
		Double res = formProc.process(formula);
		assertEquals(res, new Double(8));
	}

}